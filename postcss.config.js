module.exports = {
    plugins: [
        require('postcss-import'),
        require('tailwindcss')('tailwind.js'),
        require('autoprefixer'),
        require('@fullhuman/postcss-purgecss')({
            content: [
                'src/**/*.php',
                'resources/**/*.js',
                'resources/**/*.vue',
                'resources/**/*.php',
                'node_modules/vue-datetime/**/*.js',
                'node_modules/vue-select/**/*.js',
                'node_modules/sweetalert2/**/*.js',
            ],
        }),
        require('cssnano'),
    ],
};