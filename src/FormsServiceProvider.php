<?php

namespace Setwise\Forms;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Setwise\Forms\Services\FormBinder;

class FormsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services
     */
    public function boot()
    {
        // Load package assets
         $this->loadViewsFrom(__DIR__ . '/../resources/views', 'setwise-forms');

        if ($this->app->runningInConsole()) {
            // Publishing the config
            $this->publishes([
                __DIR__ . '/../config/setwise-forms.php' => config_path('setwise-forms.php'),
            ], 'setwise-forms-config');

            // Publishing the views
            $this->publishes([
                __DIR__ . '/../resources/views' => resource_path('views/vendor/setwise-forms'),
            ], 'setwise-forms-views');
        }

        $this->bootMacros();
        $this->bootBladeDirectives();

        foreach (config('setwise-forms.components', []) as $alias => $class) {
            Blade::component($class, $alias);
        }
    }

    /**
     * Register the application services
     */
    public function register()
    {
        // Apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/setwise-forms.php', 'setwise-forms');

        // Build FormBinder singleton
        $this->app->singleton(FormBinder::class, fn() => new FormBinder());
    }

    /**
     * Register package's macros
     *
     * @return void
     */
    protected function bootMacros()
    {
        Str::macro('snakeToTitle', function ($value) {
            return Str::title(str_replace('_', ' ', $value));
        });
    }

    /**
     * Register package's directives
     *
     * @return void
     */
    protected function bootBladeDirectives()
    {
        Blade::directive('bind', function ($data) {
            return "<?php app(\Setwise\Forms\Services\FormBinder::class)->bindForm({$data}); ?>";
        });

        Blade::directive('endbind', function () {
            return '<?php app(\Setwise\Forms\Services\FormBinder::class)->pop(); ?>';
        });
    }
}
