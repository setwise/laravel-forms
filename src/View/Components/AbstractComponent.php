<?php

namespace Setwise\Forms\View\Components;

use Illuminate\View\Component;

abstract class AbstractComponent extends Component
{
    /**
     * @return \Illuminate\View\View|string
     */
    abstract public function render();

    /**
     * @param $key
     * @param string $default
     *
     * @return string
     */
    public function attribute($key, $default = '')
    {
        return $this->attributes[$key] ?? $default;
    }

    /**
     * @param array $items
     *
     * @return string
     */
    protected function implode(array $items = [])
    {
        return implode(" ", $items);
    }
}
