<?php

namespace Setwise\Forms\View\Components\Form;

class FormComponent extends BaseComponent
{
    /** @var string HTTP Method */
    public $method = 'POST';

    /** @var string URL Target */
    public $action = '';

    /** @var bool With CSRF protection */
    public $withCSRF = true;

    /**
     * FormComponent constructor.
     *
     * @param string $method
     * @param string $action
     * @param bool $withCSRF
     */
    public function __construct(
        string $method,
        string $action,
        bool $withCSRF = true
    ) {
        $this->method = strtoupper($method);
        $this->action = $action;
        $this->withCSRF = $withCSRF;
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return 'setwise-forms::components.forms.form';
    }
}
