<?php

namespace Setwise\Forms\View\Components\Form;

use Setwise\Forms\View\Components\AbstractComponent;

abstract class BaseComponent extends AbstractComponent
{
    public const ORIENTATION_VERTICAL = 'vertical';
    public const ORIENTATION_HORIZONTAL = 'horizontal';

    /**
     * @return bool
     */
    public function isVertical()
    {
        return config('setwise-forms.orientation', self::ORIENTATION_VERTICAL) == self::ORIENTATION_VERTICAL;
    }

    /**
     * @return bool
     */
    public function isHorizontal()
    {
        return config('setwise-forms.orientation', self::ORIENTATION_HORIZONTAL) == self::ORIENTATION_HORIZONTAL;
    }
}
