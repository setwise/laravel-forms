<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

class ToggleableComponent extends Input
{
    /**
     * @inheritDoc
     */
    public function renderComponent()
    {
        return 'setwise-forms::components.forms.inputs.toggleable';
    }
}
