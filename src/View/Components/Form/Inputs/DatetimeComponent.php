<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

class DatetimeComponent extends Input
{
    /**
     * @inheritDoc
     */
    public function renderComponent()
    {
        return 'setwise-forms::components.forms.inputs.datetime';
    }
}
