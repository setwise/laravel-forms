<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

use Illuminate\Support\Str;
use Setwise\Forms\View\Components\Form\BaseComponent;

class SubmitComponent extends BaseComponent
{
    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return 'setwise-forms::components.forms.inputs.submit';
    }
}
