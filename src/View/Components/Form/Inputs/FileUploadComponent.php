<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

use Setwise\Forms\Services\FormBinder;

class FileUploadComponent extends Input
{

    /** @var string */
    public $url;

    /** @var string */
    public $deleteUrl;

    public function __construct(
        FormBinder $forms,
        string $name,
        string $url,
        string $deleteUrl = '',
        string $id = '',
        string $label = '',
        string $errorName = '',
        string $errorBag = 'default',
        $defaultValue = null,
        bool $withErrors = true,
        bool $withLabel = true,
        $readonly = false,
        $bind = true
    ) {
        $this->url = $url;
        $this->deleteUrl = $deleteUrl;

        parent::__construct(
            $forms,
            $name,
            $id,
            $label,
            $errorName,
            $errorBag,
            $defaultValue,
            $withErrors,
            $withLabel,
            $readonly,
            $bind
        );
    }

    /**
     * @inheritDoc
     */
    public function renderComponent()
    {
        return 'setwise-forms::components.forms.inputs.file_upload';
    }
}
