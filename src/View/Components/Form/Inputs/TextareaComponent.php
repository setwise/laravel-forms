<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

class TextareaComponent extends Input
{

    /**
     * @inheritDoc
     */
    public function inputClass()
    {
        return 'h-full ' . parent::inputClass();
    }

    /**
     * @inheritDoc
     */
    public function renderComponent()
    {
        return 'setwise-forms::components.forms.inputs.textarea';
    }
}
