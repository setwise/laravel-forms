<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

class CheckboxComponent extends Input
{
    /**
     * @return string
     */
    public function isChecked()
    {
        return $this->old == '1' ? 'checked="checked"' : '';
    }

    /**
     * @inheritDoc
     */
    public function inputWrapperClass()
    {
        return 'pt-1 ' . parent::inputWrapperClass();
    }

    /**
     * @inheritDoc
     */
    public function inputClass()
    {
        $classes[] = 'form-checkbox cursor-pointer';

        return $this->implode($classes);
    }

    /**
     * @inheritDoc
     */
    public function renderComponent()
    {
        return 'setwise-forms::components.forms.inputs.checkbox';
    }
}
