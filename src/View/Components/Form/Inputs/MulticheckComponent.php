<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

use Setwise\Forms\Services\FormBinder;

class MulticheckComponent extends Input
{

    /** @var \Illuminate\Support\Collection */
    public $options;

    public function __construct(
        FormBinder $forms,
        $options,
        string $name,
        string $id = '',
        string $label = '',
        string $errorName = '',
        string $errorBag = 'default',
        $defaultValue = null,
        bool $withErrors = true,
        bool $withLabel = true,
        $readonly = false,
        $bind = true
    ) {
        $this->options = is_array($options) ? collect($options) : $options;

        parent::__construct(
            $forms,
            $name,
            $id,
            $label,
            $errorName,
            $errorBag,
            $defaultValue,
            $withErrors,
            $withLabel,
            $readonly,
            $bind
        );
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function isChecked(string $value)
    {
        if (!is_array($this->old)) {
            return $this->old == $value ? 'checked="checked"' : '';
        } else {
            return in_array($value, $this->old ?: []) ? 'checked="checked"' : '';
        }
    }

    /**
     * @inheritDoc
     */
    public function inputWrapperClass()
    {
        return 'pt-1 ' . parent::inputWrapperClass();
    }

    /**
     * @return string
     */
    public function inputClass()
    {
        $classes[] = 'form-checkbox cursor-pointer';

        return $this->implode($classes);
    }

    /**
     * @inheritDoc
     */
    public function renderComponent()
    {
        return 'setwise-forms::components.forms.inputs.multicheck';
    }
}
