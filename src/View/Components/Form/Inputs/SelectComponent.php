<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

use Setwise\Forms\Services\FormBinder;

class SelectComponent extends Input
{

    /** @var \Illuminate\Support\Collection */
    public $options;

    /** @var bool */
    public $multiple;

    /** @var bool */
    public $taggable;

    public function __construct(
        FormBinder $forms,
        $options,
        string $name,
        string $id = '',
        string $label = '',
        string $errorName = '',
        string $errorBag = 'default',
        $defaultValue = null,
        bool $withErrors = true,
        bool $withLabel = true,
        $readonly = false,
        $bind = true,
        bool $multiple = false,
        bool $taggable = false
    ) {
        $this->options = is_array($options) ? collect($options) : $options;
        $this->multiple = $multiple;
        $this->taggable = $taggable;

        parent::__construct(
            $forms,
            $name,
            $id,
            $label,
            $errorName,
            $errorBag,
            $defaultValue,
            $withErrors,
            $withLabel,
            $readonly,
            $bind
        );
    }


    /**
     * @param $name
     * @param $defaultValue
     * @return mixed
     */
    protected function getOld($name, $defaultValue)
    {
        $old = parent::getOld($name, $defaultValue);

        if ($old) {
            if ($this->multiple) {
                $newOld = [];
                foreach ($old as $item) {
                    $value = $this->taggable ?
                        $item :
                        $this->optionsFormatted()->firstWhere('value', '=', $item);

                    if ($value) {
                        $newOld[] = $value;
                    }
                }
                $old = $newOld;
            } else {
                $old = $this->taggable ?
                    $old :
                    $this->optionsFormatted()->firstWhere('value', '=', $old);
            }
        }

        return $old;
    }

    /**
     * Return a formatted option list
     *
     * @return \Illuminate\Support\Collection
     */
    public function optionsFormatted()
    {
        if ($this->taggable) {
            return $this->options->values();
        } else {
            return $this->options->map(function ($option, $key) {
                return
                    [
                        'label' => $option,
                        'value' => $key,
                    ];
            })->values();
        }
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function isSelected($value)
    {
        $selected = $this->multiple && is_array($this->old)
            ? in_array($value, $this->old) :
            (optional($this->old)['value'] == $value || $this->old == $value);

        return $selected ? 'selected="selected"' : '';
    }

    /**
     * @inheritDoc
     */
    public function inputClass()
    {
        $classes[] = 'form-input shadow rounded p-0 w-full';

        if ($this->readonly) {
            $classes[] = 'disabled';
        }

        return $this->implode($classes);
    }

    /**
     * @inheritDoc
     */
    public function renderComponent()
    {
        return 'setwise-forms::components.forms.inputs.select';
    }
}
