<?php

namespace Setwise\Forms\View\Components\Form\Inputs;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Setwise\Forms\Services\FormBinder;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\ViewErrorBag;

abstract class Input extends BaseComponent
{
    /** @var mixed */
    public $binding;

    /** @var string */
    public $name;

    /** @var string */
    public $id;

    /** @var string */
    public $label;

    /** @var bool */
    public $showErrors;

    /** @var string */
    public $errorBag;

    /** @var string */
    public $errorName;

    /** @var bool */
    public $showLabel;

    /** @var bool|string */
    public $readonly;

    /** @var mixed */
    public $old;

    /**
     * Return the form component's path string
     *
     * @return string
     */
    abstract public function renderComponent();

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return 'setwise-forms::components.forms.inputs.base';
    }

    /**
     * @param \Setwise\Forms\Services\FormBinder $forms
     * @param string $name
     * @param string $id
     * @param string $label
     * @param string $errorName
     * @param string $errorBag
     * @param mixed|null $defaultValue
     * @param bool $withErrors
     * @param bool $withLabel
     * @param string|bool $readonly
     * @param bool|object|null $bind
     */
    public function __construct(
        FormBinder $forms,
        string $name,
        string $id = '',
        string $label = '',
        string $errorName = '',
        string $errorBag = 'default',
        $defaultValue = null,
        bool $withErrors = true,
        bool $withLabel = true,
        $readonly = false,
        $bind = true
    ) {
        $this->name = $name;
        $this->id = $id ?: Str::random(6);
        $this->label = $label ?: Str::snakeToTitle($name);
        $this->errorName = $errorName ?: str_replace(']', '', str_replace('[', '.', $name));
        $this->errorBag = $errorBag;
        $this->showErrors = $withErrors && Session::get('errors', app(ViewErrorBag::class))
                ->getBag($errorBag)->has($this->errorName);
        $this->showLabel = $withLabel;
        $this->readonly = $readonly;

        if ($bind === true) {
            $this->binding = $forms->getForm();
        } elseif ($bind !== false) {
            $this->binding = $bind;
        } else {
            $this->binding = null;
        }

        $this->old = $this->getOld($this->errorName, $defaultValue);
    }

    /**
     * @param $name
     * @param $defaultValue
     * @return mixed
     */
    protected function getOld($name, $defaultValue)
    {
        $oldDefault = null;
        if ($defaultValue !== null) {
            $oldDefault = $defaultValue;
        } elseif ($this->binding) {
            // Try to pull the model's attribute
            $old = Arr::get($this->binding, $name);

            if ($old instanceof Collection) {
                // If it is a instance of collection, we've got an eloquent relation, map by key
                $old = $old->modelKeys();
            } elseif ($old instanceof Model) {
                // Else it is an eloquent model, grab the key
                $old = $old->getKey();
            }

            $oldDefault = $old;
        }

        return old($name, $oldDefault);
    }

    /**
     * @return string
     */
    public function labelClass()
    {
        $classes[] = 'font-semibold block';

        switch (config('setwise-forms.orientation', self::ORIENTATION_VERTICAL)) {
            case self::ORIENTATION_HORIZONTAL:
                $classes[] = 'mr-2';
                $classes[] = 'w-full';

                $classes[] = 'sm:mt-1';
                $classes[] = 'sm:w-32';

                $classes[] = 'md:w-40';
                break;
            case self::ORIENTATION_VERTICAL:
                $classes[] = 'mb-1';
                break;
        }

        return $this->implode($classes);
    }

    /**
     * @return string
     */
    public function inputWrapperClass()
    {
        $classes[] = 'flex-1';

        if ($this->showErrors) {
            $classes[] = 'input-red';
        }

        return $this->implode($classes);
    }

    /**
     * @return string
     */
    public function inputClass()
    {
        $classes[] = 'form-input shadow w-full py-1';

        if ($this->readonly) {
            $classes[] = 'disabled';
        }

        if ($this->showErrors) {
            $classes[] = 'border-red-500';
        }

        return $this->implode($classes);
    }
}
