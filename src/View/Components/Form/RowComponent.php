<?php

namespace Setwise\Forms\View\Components\Form;

class RowComponent extends BaseComponent
{
    /**
     * Return the default class
     *
     * @return string
     */
    public function defaultClass()
    {
        $classes[] = 'flex flex-row flex-wrap space-x-4';

        return $this->implode($classes);
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return 'setwise-forms::components.forms.row';
    }
}
