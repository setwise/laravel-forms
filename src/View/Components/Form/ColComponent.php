<?php

namespace Setwise\Forms\View\Components\Form;

class ColComponent extends BaseComponent
{
    /**
     * Return the default class
     *
     * @return string
     */
    public function defaultClass()
    {
        $classes[] = 'flex flex-col flex-wrap space-y-3';

        return $this->implode($classes);
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return 'setwise-forms::components.forms.col';
    }
}
