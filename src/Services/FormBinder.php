<?php

namespace Setwise\Forms\Services;

use Setwise\Forms\View\Components\Form\BaseComponent;

class FormBinder
{
    /**
     * $forms is a collection which is processed as a stack
     * Forms are added and removed from the stack
     *
     * @var \Illuminate\Support\Collection
     */
    protected $forms = null;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->forms = collect([]);
    }

    /**
     * Add a form to the list of active forms
     *
     * @param object|array|null $model
     * @return $this
     */
    public function bindForm($model = null)
    {
        $this->forms->push($model);

        return $this;
    }

    /**
     * Remove the last form from the list
     *
     * @return mixed
     */
    public function pop()
    {
        return $this->forms->pop();
    }

    /**
     * Get the last form added to the list
     *
     * @return mixed
     */
    public function getForm()
    {
        return $this->forms->last();
    }
}
