<?php

use Setwise\Forms\View\Components;

return [
    /**
     * Orientation for all forms within the application
     */
    'orientation' => Components\Form\BaseComponent::ORIENTATION_HORIZONTAL,

    // List of components to be registered
    'components' => [
        // Base Form
        'form' => Components\Form\FormComponent::class,

        // Rows / Cols
        'form.col' => Components\Form\ColComponent::class,
        'form.row' => Components\Form\RowComponent::class,

        // Inputs
        'form.checkbox' => Components\Form\Inputs\CheckboxComponent::class,
        'form.base' => Components\Form\Inputs\Input::class,
        'form.ckeditor' => Components\Form\Inputs\CKEditorComponent::class,
        'form.datetime' => Components\Form\Inputs\DatetimeComponent::class,
        'form.file.upload' => Components\Form\Inputs\FileUploadComponent::class,
        'form.image.upload' => Components\Form\Inputs\ImageUploadComponent::class,
        'form.input' => Components\Form\Inputs\InputComponent::class,
        'form.multicheck' => Components\Form\Inputs\MulticheckComponent::class,
        'form.radio' => Components\Form\Inputs\RadioComponent::class,
        'form.select' => Components\Form\Inputs\SelectComponent::class,
        'form.submit' => Components\Form\Inputs\SubmitComponent::class,
        'form.textarea' => Components\Form\Inputs\TextareaComponent::class,
        'form.toggleable' => Components\Form\Inputs\ToggleableComponent::class,
    ]
];
