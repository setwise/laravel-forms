
# Setwise Forms  
  
This package aims to consolidate all Setwise forms into a single, cohesive package.  
The package comes with many out-of-the-box blade components and their associated Vue components if necessary.
You may optionally extend individual components or add new components using various Laravel publish commands 
  
### Installation  
  
Install via the command  
```bash  
composer require setwise/laravel-forms  
```  
  
### Configuration  

Setwise Forms comes with a bit of front end configuration through Laravel mix, app.js, and app.css.
Below is a quick step through of the required configuration steps.  
  
1) Modify `webpack.mix.js` to add an alias for `setwise-forms`.
This allows you to reference components via `import '' from 'setwise-forms'`.
```js
mix.webpackConfig({
    resolve: {
        alias: {
            'setwise-forms': path.resolve('vendor/setwise/laravel-forms/dist/main.esm'),
        },
    },
});
```

2) Modify `webpack.mix.js` to copy over the css assets for `setwise-forms`
```js
mix.copy('vendor/setwise/laravel-forms/dist/main.css', 'public/css/setwise-forms.css');
```

3) Include the css published this way in your layouts file
```html
<link rel="stylesheet preload"
      as="style"
      type="text/css"
      crossorigin="anonymous"
      href="{{ asset('css/setwise-forms.css') }}">
```

4) Install Vue components onto global Vue instance by adding the below two lines to `app.js`.
```js
import {SetwiseForms} from 'setwise-forms';

Vue.use(SetwiseForms);
```
  
5) Install component styling by adding the below line to `app.css`. This does require the `postcss-loader` package to be installed via `yarn add postcss-loader`.
```css
@import './../../vendor/setwise/laravel-forms/dist/main.css';
```
  
6) OPTIONAL* Two commands are available to publish aspects of this package.
```bash  
php artisan vendor:publish --tag=setwise-forms-config  
php artisan vendor:publish --tag=setwise-forms-views
```  
  
##### Adding Additional Components  
  
This package registers all components automatically using the config key `forms.components`  
The below array would register `x-button` and `x-alert` to the associated `ButtonComponent::class` and `AlertComponen::class`  
```php  
<?php  
  
return [  
    'components' => [  
        'button' => ButtonComponent::class,  
        'alert' => AlertComponent::class,  
    ],  
];  
```  
  
### Form Components  
  
This package works to provide standard form components.  
By default, the following form components are available...

 ---
 
### Tags  
  
Key  
- '*': Required  
- '(Value)': Default Value  

---
  
### Models  
Because models being displayed can be redundant, the label and value wrappers have been abstracted  

#### `x-model.row`  
1. Label (Default ''): Label string to display  
2. Value (Default: ''): Value to display (You can optionally use the $slot to pass the same value)  
3. Orientation (Defaults to horizontal): Orientation of the field  
  
Example:  
```blade  
<x-model.row label="First Name">  
        {{ $user->first }}  
</x-model.row>  
  
<x-model.row label="Last Name" :value="$user->last"></x-model.row>  
```
  
---
  
### Forms  
  
#### `x-form`  
1. Method*: Request type [GET, POST, PATCH, PUT, DELETE]  
2. Action*: Target URL  
3. WithCSRF (True): If the form requires CSRF protection  
4. $slot: Form innerHTML  
  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    @include('users.form')  
</x-form>  
```  

#### `@bind` and `@unbind`
The @bind blade tag can be used to bind a Model to a form in order for future fields to be populated by default.
```
<x-form method="patch" action="{{ route('users.update") }}>
    @bind($user)
        @include('users.form')
    @unbind
</x-form>
```
  
#### `x-form.row`  
1. $slot: Row innerHTML  
  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-form.row>  
        Hello World  
    </x-form.row>  
</x-form>  
```  
  
#### `x-form.col`  
1. $slot: Column innerHTML  
  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-form.col>  
        Hello World  
    </x-form.col>  
</x-form>  
```  

---
  
### Input Tags  
The below components all inherit from the same input object and utilize its properties  
1. Name*: Name of the given form field  
2. Id (6 Char Random): Id for the field  
3. Label (Formatted `$name`): Label for the field  
4. ErrorName (Formatted `$name`): Name of the element if it were in an error bag  
5. ErrorBag ('default'): Name of the element if it were in an error bag  
5. DefaultValue (Tries to use `@bind` and old() from the parent form): Default value for field
6. WithErrors (True): If validation error text should be displayed with red borders  
7. WithLabel (True): If the label should be displayed  
8. Readonly (False): If the element is readonly  
9. Bind (True): If the element should fetch values from the `@bind` model
  
#### `x-input.input`  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.input type="email" name="email"></x-input.input>  
</x-form>  
```  
  
#### `x-input.textarea`  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.textarea placeholder="Application Notes..." name="notes"></x-input.textarea>  
</x-form>  
```  
  
#### `x-input.toggleable`  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.toggleable name="password"></x-input.toggleable>  
</x-form>  
```  
  
#### `x-input.image.upload`  
1) Url*: Upload endpoint  
2) DeleteEndpoint*: Deletion endpoint  
  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.upload.image url="UPLOAD ENDPOINT" delete-url="DELETE ENDPOINT"  
        name="description">  
    </x-input.upload.image>  
</x-form>  
```  
  
#### `x-input.file.upload`  
1) Url*: Upload endpoint  
2) DeleteEndpoint*: Deletion endpoint  
  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.upload.file url="UPLOAD ENDPOINT" delete-url="DELETE ENDPOINT"  
        name="description">  
    </x-input.upload.file>  
</x-form>  
```  
  
#### `x-input.checkbox`    
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.checkbox name="active"></x-input.checkbox>  
</x-form>  
```  

#### `x-input.radio`    
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.radio name="active" :options=['1' => 'Hello', '2' => 'World']></x-input.radio>  
</x-form>  
```  
  
#### `x-input.select`  
1) Options*: Options to select from  
2) Multiple (False): If multiple can be selected  
3) Taggable (False): If tagged entries can be selected  
  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.select name="select_mult" :options="$options" :multiple="true"></x-input.select>  
     
    <x-input.select name="select_sing" :options="$options"></x-input.select>  
</x-form>  
```  
  
#### `x-input.multicheck`  
1) Options*: Options to check from  
  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.multicheck :options="$options" inline="inline"></x-input.multicheck>  
  </x-form>  
```  
  
#### `x-input.datetime`  
Example:  
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.datetime name="datetime" type="datetime"></x-input.datetime>  
</x-form>  
```
  
#### `x-input.submit`  
1. Type ('green'): Button color  
2. $slot: Button innerHTML  
  
Example:   
```blade  
<x-form method="post" action="{{ route('store') }}">  
    <x-input.submit>  
        <span class="fa fa-submit"></span> Click Me!!!  
    </x-input.submit>  
</x-form>  
```  
  
---

### Testing  
  
``` bash  
composer test  
```  
  
### Changelog  
  
Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.  
  
## Credits  
  
- [Nicholas Dykhuizen](https://bitbucket.com/setwise)