import VSelect from 'vue-select';
import {Datetime} from 'vue-datetime';
import InfiniteScroll from './Components/InfiniteScroll.vue';
import ToggleableInput from './Components/ToggleableInput.vue';
import FileUpload from './Components/FileUpload.vue';
import ImageUpload from './Components/ImageUpload.vue';

const registrations = {
    'v-select': VSelect,
    'datetime': Datetime,
    'infinite-scroll': InfiniteScroll,
    'toggleable-input': ToggleableInput,
    'file-upload': FileUpload,
    'image-upload': ImageUpload,
}

// Declare install function executed by Vue.use()
let installed = false;
export function SetwiseForms(Vue) {
    if(installed) {
        return;
    }

    installed = true;

    for(const name in registrations) {
        if (registrations.hasOwnProperty(name)) {
            Vue.component(name, registrations[name]);
        }
    }
}

// Declare install function executed by Vue.use()
const plugin = {
    SetwiseForms,
};

// Auto-install when vue is found (eg. in browser via <script> tag)
let GlobalVue = null;
if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue;
}
if (GlobalVue) {
    GlobalVue.use(plugin);
}

// To allow use as module (npm/webpack/etc.) export component
export default {
    Datetime,
};