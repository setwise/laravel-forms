import sweetalert2 from 'sweetalert2';

const swal = sweetalert2;

export default {
    computed: {
        swal() {
            return swal;
        },
    },

    methods: {
        swalSuccess(title = 'Success', text = '') {
            return this.swal.fire({
                title: title,
                text: text,
                icon: 'success',
                timer: 2000,
            });
        },
        swalError(title = 'Error', text = '') {
            return this.swal.fire({
                title: title,
                text: text,
                icon: 'error',
                timer: 2000,
            });
        },
        swalConfirm(title = 'Confirm', text = '') {
            return this.swal.fire({
                title: title,
                text: text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirm',
            });
        },
        swalLoading(title = 'Loading', text = '') {
            return this.swal.fire({
                title: title,
                text: text,
                showConfirmButton: false,
                didOpen: () => {
                    this.swal.showLoading();
                },
            });
        },
        swalClose() {
            this.swal.close();
        },
    },
};