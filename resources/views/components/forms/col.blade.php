<div class="{{ $attributes['class'] ?? $defaultClass }}"
        {{ $attributes->except(['class'])->merge([]) }}>
    {{ $slot }}
</div>