<input class="hidden"
       name="{{ $name }}"
       type="hidden"
       value="0"/>

<label class="inline-flex items-center cursor-pointer">
    <input type="checkbox"
            {{ $isChecked }}
            {{ $attributes->merge([
                'name' => $name,
                'id' => $id,
                'dusk' => $id,
                'class' => $inputClass,
                'aria-label' => $label,
                'value' => '1',
            ])->except(['type', 'checked']) }}>
    <span class="ml-2">{{ $label }}</span>
</label>