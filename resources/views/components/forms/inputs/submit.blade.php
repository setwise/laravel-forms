<div class="mt-6 flex items-center justify-between">
    <button {!! $attributes->merge([
        'class' => 'border border-green-500 bg-green-500 text-white rounded-md px-3 py-1 transition duration-500 ease select-none hover:bg-green-600 focus:outline-none focus:shadow-outline',
        'type' => 'submit',
        'aria-label' => 'Submit',
    ]) !!}>
        {!! trim($slot) ?: __('Submit') !!}
    </button>
</div>