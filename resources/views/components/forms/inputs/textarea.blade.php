<textarea {{ $attributes->merge([
                'name' => $name,
                'id' => $id,
                'dusk' => $id,
                'class' => $inputClass,
                'aria-label' => $label,
                'autocomplete' => 'off',
                'value' => $old,
            ]) }}>{{ $old }}</textarea>
