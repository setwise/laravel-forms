<div class="flex flex-row flex-wrap flex-1">
    @if($isHorizontal() && $showLabel)
        <label for="{{ $name }}" class="{{ $labelClass }}">{{ $label }}</label>
    @endif

    <div class="{{ $inputWrapperClass }}">
        @if($isVertical() && $showLabel)
            <label for="{{ $name }}" class="{{ $labelClass }}">{{ $label }}</label>
        @endif

        {{-- This is where the actual input field gets placed into --}}
        @include($renderComponent())

        <div class="flex flex-col">
            @if($showErrors)
                @include('setwise-forms::components/forms/utilities/error', [
                    'error' => $errors->getBag($errorBag)->first($errorName)
                ])
            @endif

            @isset($helpText)
                @include('setwise-forms::components/forms/utilities/help', ['text' => $helpText])
            @elseif($attribute('help-text', null))
                @include('setwise-forms::components/forms/utilities/help', ['text' => $attribute('help-text')])
            @endisset
        </div>
    </div>
</div>