<input {{ $attributes->merge([
            'name' => $name,
            'id' => $id,
            'dusk' => $id,
            'class' => $inputClass,
            'type' => 'text',
            'aria-label' => $label,
            'autocomplete' => 'off',
            'value' => $old,
        ]) }}
>
