<infinite-scroll {{ $attributes->merge([
                        'name' => $name,
                        'id' => $id,
                        'dusk' => $id,
                        'class' => $inputClass,
                        'aria-label' => $label,
                     ])->except(['options', 'value']) }}
                 :multiple="{{ $multiple ? 'true' : 'false' }}"
                 :taggable="{{ $taggable ? 'true' : 'false' }}"
                 :options="{{ $optionsFormatted()->toJson() }}"
                 :value="{{ json_encode($old) }}">
</infinite-scroll>
