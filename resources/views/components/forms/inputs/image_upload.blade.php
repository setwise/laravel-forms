<image-upload {{ $attributes->merge([
            'name' => $name,
            'id' => $id,
            'dusk' => $id,
            'input-class' => $inputClass,
            'aria-label' => $label,
            'value' => $old,
        ]) }}
             url="{{ $url }}"
             delete-url="{{ $deleteUrl }}">
</image-upload>
