{{-- Apply default for checkbox set --}}
<input class="hidden"
       name="{{ $name }}"
       type="hidden"
       value="">

@foreach($options as $key => $option)
    <div>
        <label class="inline-flex items-center">
            <input
                    type="checkbox"
                    {{ $isChecked($key) }}
                    name="{{ $name }}[]"
                    value="{{ $key }}"
                    {{ $attributes->merge([
                        'id' => "{$id}-{$key}",
                        'dusk' => $id,
                        'class' => $inputClass,
                        'aria-label' => $label,
                    ])->except(['type', 'checked', 'name', 'value']) }}>
            <span class="ml-2">{{ $option }}</span>
        </label>
    </div>
@endforeach