<div class="flex flex-row items-center">
    <div class="flex-1">
        <datetime ref="{{ $id }}"
                {{ $attributes->merge([
                    'name' => $name,
                    'input-id' => $id,
                    'dusk' => $id,
                    'input-class' => $inputClass,
                    'aria-label' => $label,
                    'value' => $old ? Carbon\Carbon::parse($old)->toISOString(true) : '',
                    'zone' => config('app.timezone'),
                    'value-zone' => config('app.timezone'),
                    ':use12-hour' => 'true',
                ])->except(['class', 'ref']) }}>
        </datetime>
    </div>
    <div class="cursor-pointer ml-2" @click="() => { $refs['{{ $id }}'].datetime = null }">
        <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                  d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
        </svg>
    </div>
</div>
