<toggleable-input {{ $attributes->merge([
                    'name' => $name,
                    'id' => $id,
                    'dusk' => $id,
                    'input-class' => $inputClass,
                    'aria-label' => $label,
                    'value' => $old,
                ]) }}>
</toggleable-input>
