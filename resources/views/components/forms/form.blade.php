<form action="{{ $action }}"
      method="{{ in_array(strtoupper($method), ['POST', 'PUT', 'PATCH', 'DELETE']) ? 'POST' : 'GET' }}"
      class="{{ $attributes['class'] ?? 'flex flex-col p-4 space-y-4' }}"
        {{ $attributes->except(['action', 'method', 'class'])->merge([]) }}>
    @if(in_array(strtoupper($method), ['PUT', 'PATCH', 'DELETE']))
        @method($method)
    @endif

    @if($withCSRF)
        @csrf
    @endif

    {{ $slot }}
</form>