import buble from '@rollup/plugin-buble'; // Transpile/polyfill with reasonable browser support
import commonjs from '@rollup/plugin-commonjs'; // Convert CommonJS modules to ES6
import {terser} from 'rollup-plugin-terser';
import vue from 'rollup-plugin-vue'; // Handle .vue SFC files

export default {
    input: 'resources/js/main.js', // Path relative to package.json
    output: [
        {
            name: 'SetwiseForms',
            exports: 'named',
            file: 'dist/main.umd.js',
            format: 'umd',
            globals: {
                'vue-select': 'VSelect',
                'vue-datetime': 'VueDatetime',
                'axios': 'axios',
                'sweetalert2': 'sweetalert2',
            },
        },
        {
            name: 'SetwiseForms',
            exports: 'named',
            file: 'dist/main.esm.js',
            format: 'es',
            globals: {
                'vue-select': 'VSelect',
                'vue-datetime': 'VueDatetime',
                'axios': 'axios',
                'sweetalert2': 'sweetalert2',
            },
        },
        {
            name: 'SetwiseForms',
            exports: 'named',
            file: 'dist/main.min.js',
            format: 'iife',
            globals: {
                'vue-select': 'VSelect',
                'vue-datetime': 'VueDatetime',
                'axios': 'axios',
                'sweetalert2': 'sweetalert2',
            },
        },
    ],
    plugins: [
        commonjs(),
        vue({
            css: false, // Dynamically inject css as a <style> tag
            compileTemplate: true, // Explicitly convert template to render function
        }),
        buble(), // Transpile to ES5
        terser(),
    ],
    external: ['vue-datetime', 'vue-select', 'axios', 'sweetalert2'],
};