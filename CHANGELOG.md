# Changelog
All notable changes to `forms` will be documented in this file

## Development

## 2.0.0 - November 17th, 2020
- Added contained Vue and styling components
- Added error bag option to inputs
- Rebuilt internal component rendering
- Changed `x-form.text` to `x-form.input`
- Changed checkbox variables to not allow for radio options
- Added `x-form.radio` component
- Updated the documentation to reflect how to add dependencies to front end 

## 1.0.4 - July 29th, 2020
- Open sourced package on packagist
- `x-multicheck` radio and checkbox types failed to display default values properly

## 1.0.3 - July 13th, 2020
- Fixed `x-select` when using taggable
- Adjusted `x-datetime` to center trash icon with input rather than label & input
- Removed `x-seo.rows` form component
- Moved `x-seo.rows` to `setwise/laravel-seo` package

## 1.0.0 - July 8th, 2020
- Initial release
