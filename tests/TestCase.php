<?php

namespace Tests;

use Illuminate\View\ComponentAttributeBag;
use Illuminate\View\View;
use Orchestra\Testbench\TestCase as TestBench;
use Setwise\Forms\FormsServiceProvider;

abstract class TestCase extends TestBench
{
    protected function getPackageProviders($app)
    {
        return [FormsServiceProvider::class];
    }

    /**
     * Create a new TestView from the given view component.
     *
     * @param string $viewComponent
     * @param \Illuminate\Contracts\Support\Arrayable|array $data
     * @param array $attributes
     * @return \Tests\TestView
     */
    protected function component(string $viewComponent, array $data = [], array $attributes = [])
    {
        $component = $this->app->make($viewComponent, $data);
        $view = $component->resolveView();

        $data = array_merge($data, $component->data(), ['attributes' => new ComponentAttributeBag($attributes)]);
        if ($view instanceof View) {
            return new TestView($view->with($data));
        }

        return new TestView(
            view($view, array_merge($data))
        );
    }
}
