<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Setwise\Forms\View\Components\AbstractComponent;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Setwise\Forms\View\Components\Form\FormComponent;
use Setwise\Forms\View\Components\Form\Inputs\InputComponent;
use Setwise\Forms\View\Components\Form\Inputs\ToggleableComponent;

class TextComponentTest extends TestCase
{
    public function testRender()
    {
        $this->component(InputComponent::class, [
            'name' => 'testing',
        ])->assertSee('testing')
            ->assertSee('input');
    }
}
