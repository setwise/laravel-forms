<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Setwise\Forms\View\Components\AbstractComponent;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Setwise\Forms\View\Components\Form\FormComponent;
use Setwise\Forms\View\Components\Form\Inputs\DatetimeComponent;

class DatetimeComponentTest extends TestCase
{
    public function testRender()
    {
        $this->component(DatetimeComponent::class, [
            'name' => 'element'
        ])->assertSee('datetime')
            ->assertSee('value-zone');
    }
}
