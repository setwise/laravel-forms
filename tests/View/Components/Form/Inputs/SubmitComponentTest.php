<?php

namespace Tests\View\Components\Form\Inputs;

use Tests\TestCase;
use Setwise\Forms\View\Components\Form\Inputs\SubmitComponent;

class SubmitComponentTest extends TestCase
{
    public function testRender()
    {
        $this->component(SubmitComponent::class, [
            'slot' => 'Submit',
        ])->assertSee('bg-green-500');
    }
}
