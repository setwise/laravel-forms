<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Setwise\Forms\View\Components\Form\Inputs\RadioComponent;
use Tests\TestCase;
use Setwise\Forms\View\Components\AbstractComponent;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Setwise\Forms\View\Components\Form\FormComponent;
use Setwise\Forms\View\Components\Form\Inputs\MulticheckComponent;

class RadioComponentTest extends TestCase
{
    public function testIsChecked()
    {
        $view = $this->component(RadioComponent::class, [
            'options' => [1 => 'Hello', 2 => 'World'],
            'name' => 'radio',
            'defaultValue' => 1,
        ])->getView();
        $this->assertStringContainsString('checked', ($view->getData()['isChecked'](1)));
        $this->assertEquals('', $view->getData()['isChecked'](2));

        $view = $this->component(RadioComponent::class, [
            'options' => [1 => 'Hello', 2 => 'World'],
            'name' => 'radio',
            'defaultValue' => [1],
        ])->getView();
        $this->assertStringContainsString('checked', ($view->getData()['isChecked'](1)));
        $this->assertEquals('', $view->getData()['isChecked'](2));
    }

    public function testRender()
    {
        $this->component(RadioComponent::class, [
            'options' => [1 => 'Hello', 2 => 'World'],
            'name' => 'multicheck',
            'type' => 'checkbox'
        ])->assertSee('Hello')
            ->assertSee('World')
            ->assertSee('radio');
    }
}
