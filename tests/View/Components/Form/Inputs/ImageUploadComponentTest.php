<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Setwise\Forms\View\Components\AbstractComponent;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Setwise\Forms\View\Components\Form\FormComponent;
use Setwise\Forms\View\Components\Form\Inputs\ImageUploadComponent;
use Setwise\Forms\View\Components\Form\Inputs\UploadComponent;

class ImageUploadComponentTest extends TestCase
{
    public function testRender()
    {
        $url = 'https://testing.com';
        $deleteUrl = 'https://delete.com';

        $this->component(ImageUploadComponent::class, [
            'name' => 'testing',
            'url' => $url,
            'deleteUrl' => $deleteUrl,
        ])->assertSee($url)
            ->assertSee($deleteUrl)
            ->assertSee('upload');
    }
}
