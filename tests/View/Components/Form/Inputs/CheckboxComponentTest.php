<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Setwise\Forms\View\Components\Form\Inputs\CheckboxComponent;
use Tests\TestCase;

class CheckboxComponentTest extends TestCase
{
    public function testIsChecked()
    {
        $view = $this->component(CheckboxComponent::class, [
            'name' => 'element',
            'defaultValue' => true,
        ])->getView();
        $this->assertStringContainsString('checked', $view->getData()['isChecked']());

        $view = $this->component(CheckboxComponent::class, [
            'name' => 'element',
            'defaultValue' => false,
        ])->getView();
        $this->assertEquals('', $view->getData()['isChecked']());
    }

    public function testRender()
    {
        $this->component(CheckboxComponent::class, [
            'name' => 'element',
            'defaultValue' => true,
        ])->assertSee('checkbox');
    }
}
