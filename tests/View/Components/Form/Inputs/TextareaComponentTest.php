<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Setwise\Forms\View\Components\AbstractComponent;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Setwise\Forms\View\Components\Form\FormComponent;
use Setwise\Forms\View\Components\Form\Inputs\TextareaComponent;
use Setwise\Forms\View\Components\Form\Inputs\InputComponent;

class TextareaComponentTest extends TestCase
{
    public function testRender()
    {
        $this->component(TextareaComponent::class, [
            'name' => 'testing',
        ])->assertSee('testing')
            ->assertSee('textarea');
    }
}
