<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Setwise\Forms\View\Components\AbstractComponent;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Setwise\Forms\View\Components\Form\FormComponent;
use Setwise\Forms\View\Components\Form\Inputs\MulticheckComponent;

class MulticheckComponentTest extends TestCase
{
    public function testIsChecked()
    {
        $view = $this->component(MulticheckComponent::class, [
            'options' => [1 => 'Hello', 2 => 'World'],
            'name' => 'multicheck',
            'defaultValue' => 1,
        ])->getView();
        $this->assertStringContainsString('checked', ($view->getData()['isChecked'](1)));
        $this->assertEquals('', $view->getData()['isChecked'](2));

        $view = $this->component(MulticheckComponent::class, [
            'options' => [1 => 'Hello', 2 => 'World'],
            'name' => 'multicheck',
            'defaultValue' => [1],
        ])->getView();
        $this->assertStringContainsString('checked', ($view->getData()['isChecked'](1)));
        $this->assertEquals('', $view->getData()['isChecked'](2));
    }

    public function testRender()
    {
        $this->component(MulticheckComponent::class, [
            'options' => [1 => 'Hello', 2 => 'World'],
            'name' => 'multicheck',
            'type' => 'checkbox'
        ])->assertSee('Hello')
            ->assertSee('World')
            ->assertSee('checkbox');
    }
}
