<?php

namespace Tests\View\Components\Form\Inputs;

use Tests\TestCase;
use Setwise\Forms\View\Components\Form\Inputs\ToggleableComponent;
use Setwise\Forms\View\Components\Form\Inputs\UploadComponent;

class ToggleableComponentTest extends TestCase
{
    public function testRender()
    {
        $this->component(ToggleableComponent::class, [
            'name' => 'testing',
        ])->assertSee('testing')
            ->assertSee('toggleable-input');
    }
}
