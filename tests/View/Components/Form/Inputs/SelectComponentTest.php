<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Setwise\Forms\Services\FormBinder;
use Tests\TestCase;
use Setwise\Forms\View\Components\AbstractComponent;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Setwise\Forms\View\Components\Form\FormComponent;
use Setwise\Forms\View\Components\Form\Inputs\SelectComponent;

class SelectComponentTest extends TestCase
{
    public function testOld()
    {
        $options= [
            1 => 'Hello',
            2 => 'World',
        ];
        $component = new SelectComponent(
            app()->get(FormBinder::class),
            $options,
            'select',
            false,
            false,
            '',
            '',
            1,
            '',
            '',
        );

        $this->assertEquals([
            'label' => 'Hello', 'value' => 1
        ], $component->old);
    }

    public function testOptionsFormatted()
    {
        $options= [
            1 => 'Hello',
            2 => 'World',
        ];
        $component = new SelectComponent(
            app()->get(FormBinder::class),
            $options,
            'select',
            false,
            false,
            '',
            '',
            '',
            '',
            1,
        );

        $this->assertCount(2, $component->optionsFormatted());
        $this->assertContains([
            'label' => 'Hello', 'value' => 1
        ], $component->optionsFormatted()->toArray());
    }

    public function testIsSelected()
    {
        $options= [
            1 => 'Hello',
            2 => 'World',
        ];
        $component = new SelectComponent(
            app()->get(FormBinder::class),
            $options,
            'select',
            false,
            false,
            '',
            '',
            1,
            '',
            '',
        );

        $this->assertStringContainsString('selected', $component->isSelected(1));
        $this->assertEquals('', $component->isSelected(2));
    }

    public function testRender()
    {
        $options= [
            1 => 'Hello',
            2 => 'World',
        ];

        $this->component(SelectComponent::class, [
            'name' => 'select',
            'options' => $options
        ])->assertSee('Hello')
            ->assertSee('World')
            ->assertSee(1)
            ->assertSee(2)
            ->assertSee('infinite-scroll')
            ->assertSee(':multiple="false"', false);

        $this->component(SelectComponent::class, [
            'name' => 'select',
            'multiple' => true,
            'options' => $options
        ])->assertSee(':multiple="true"', false);
    }
}
