<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Database\Eloquent\Model;
use Setwise\Forms\View\Components\Form\Inputs\InputComponent;
use Tests\TestCase;
use Setwise\Forms\View\Components\AbstractComponent;
use Setwise\Forms\View\Components\Form\BaseComponent;
use Setwise\Forms\View\Components\Form\FormComponent;
use Setwise\Forms\View\Components\Form\Inputs\DatetimeComponent;

class InputComponentTest extends TestCase
{
    public function testRender()
    {
        $this->component(InputComponent::class, [
            'name' => 'element'
        ])->assertSee('input');
    }
}
