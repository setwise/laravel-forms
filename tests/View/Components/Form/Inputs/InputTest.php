<?php

namespace Tests\View\Components\Form\Inputs;

use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use Session;
use Setwise\Forms\Services\FormBinder;
use Tests\TestCase;
use Setwise\Forms\View\Components\Form\Inputs\Input;

class InputTest extends TestCase
{
    public function testGetOld()
    {
        $component = $this->getMockForAbstractClass(Input::class, [
            app()->get(FormBinder::class),
            'hello_world',
            '',
            '',
            '',
            '',
            5
        ]);
        $this->assertEquals(5, $component->old);

        $this->withSession([
            '_old_input.hello_world' => 1,
        ]);
        request()->setLaravelSession(session());
        $component = $this->getMockForAbstractClass(Input::class, [
            app()->get(FormBinder::class),
            'hello_world',
            '',
            '',
            '',
            '',
            5
        ]);
        $this->assertEquals(1, $component->old);
    }

    public function testLabel()
    {
        $component = $this->getMockForAbstractClass(Input::class, [
            app()->get(FormBinder::class),
            'hello_world'
        ]);

        $this->assertEquals('Hello World', $component->label);
    }

    public function testWithErrors()
    {
        $bag = app(ViewErrorBag::class);
        $bag->put(
            'default',
            new MessageBag([
                'hello_world' => [
                    'This field is required'
                ],
            ])
        );
        $this->withSession([
            'errors' => $bag
        ]);

        $component = $this->getMockForAbstractClass(Input::class, [
            app()->get(FormBinder::class),
            'hello_world'
        ]);
        $this->assertTrue($component->showErrors);
    }
}
