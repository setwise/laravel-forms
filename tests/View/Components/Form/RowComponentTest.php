<?php

namespace Tests\View\Components\Form;

use Tests\TestCase;
use Setwise\Forms\View\Components\Form\RowComponent;

class RowComponentTest extends TestCase
{
    public function testRender()
    {
        $slot = '<div>Hello World</div>';
        $view = $this->component(RowComponent::class, [
            'slot' => $slot,
        ]);

        $view->assertSee('flex-row')
            ->assertSee('space-x')
            ->assertSee($slot);
    }
}
