<?php

namespace Tests\View\Components\Form;

use Setwise\Forms\View\Components\Form\ColComponent;
use Tests\TestCase;

class ColComponentTest extends TestCase
{
    public function testRender()
    {
        $slot = '<div>Hello World</div>';
        $view = $this->component(ColComponent::class, [
            'slot' => $slot,
        ]);

        $view->assertSee('flex-col')
            ->assertSee('space-y')
            ->assertSee($slot);
    }
}
