<?php

namespace Tests\View\Components\Form;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Setwise\Forms\View\Components\Form\FormComponent;
use Tests\TestCase;

class BaseComponentTest extends TestCase
{
    public function testOrientation()
    {
        $component = new FormComponent('', '', true);

        Config::set('setwise-forms.orientation', FormComponent::ORIENTATION_HORIZONTAL);
        $this->assertTrue($component->isHorizontal());
        $this->assertFalse($component->isVertical());

        Config::set('setwise-forms.orientation', FormComponent::ORIENTATION_VERTICAL);
        $this->assertFalse($component->isHorizontal());
        $this->assertTrue($component->isVertical());
    }
}
