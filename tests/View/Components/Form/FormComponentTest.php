<?php

namespace Tests\View\Components\Form;

use Tests\TestCase;
use Setwise\Forms\View\Components\Form\FormComponent;

class FormComponentTest extends TestCase
{
    public function testRender()
    {
        $slot = '<div>Hello World</div>';
        $this->component(FormComponent::class, [
            'method' => 'POST',
            'action' => '',
            'slot' => $slot,
        ])->assertSee('_token')
            ->assertSee($slot)
            ->assertSee('POST');

        $this->component(FormComponent::class, [
            'method' => 'PATCH',
            'action' => '',
            'slot' => $slot,
        ])->assertSee('POST')
            ->assertSee('_method')
            ->assertSee('PATCH');
    }
}
