<?php

namespace Tests;

class LaravelFormsTest extends TestCase
{
    public function testGetConfigValue()
    {
        $this->assertIsArray(config('setwise-forms.components'));
    }
}
